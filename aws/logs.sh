#! /bin/bash
IP=$(ifconfig | perl -nle'/dr:(\S+)/ && print $1')
MESSAGE="Griphus Process Started IP: $IP"
echo $MESSAGE
LOGGROUP=/griphus/process
LOGSTREAM=/spot
aws logs describe-log-streams --log-group-name $LOGGROUP --log-stream-name-prefix $LOGSTREAM > /tmp/logs.json
NEXT=$(python -c "
import json
with open('/tmp/logs.json') as log_file:
    logs = json.load(log_file)
logStreams = logs['logStreams']
print(logStreams[0]['uploadSequenceToken'])
")
echo $NEXT
DATE=$(($(date +%s%N --utc)/1000000))
echo $DATE
aws logs put-log-events --log-group-name $LOGGROUP --log-stream-name $LOGSTREAM --log-events "timestamp=$DATE,message=$MESSAGE" --sequence-token $NEXT

#! /usr/bin/python

import main as griphus
import logging
import os
from sys import argv


def main():
    baseLogPath = '/tmp/logs'
    logPath = os.path.join(baseLogPath, 'griphus.log')

    if not os.path.exists(baseLogPath):
        os.makedirs(baseLogPath)
    if not os.path.exists(logPath):
        open(logPath, 'w').close()
    formatter = '%(asctime)s - %(levelname)s - %(message)s'

    logging.basicConfig(filename=logPath, filemode='w', level=logging.DEBUG, format=formatter)
    logging.info('Start Main')

    event = {
        'tileName': argv[1],
        'tmp': '/tmp',
        'griphus': 'infojsons.griphus.com.au',
        'key': 'db/allTiles.json',
        'sentinel': 'sentinel-s2-l1c',
        'dataCoveragePercentage': argv[2],
        'cloudyPixelPercentage': argv[3],
        'PYTHONPATH': '',
        # os.path.join(os.path.dirname(os.path.abspath(__file__)),'lambda/local/lib64/python2.7/dist-packages'),
        'LD_LIBRARY_PATH': '',  # os.path.join(os.path.dirname(os.path.abspath(__file__)), 'lambda/local/lib'),
        'colorprofile': os.path.join(os.path.dirname(os.path.abspath(__file__)), 'profiles', 'color-relief-0-1.txt'),
        'gdaldem': 'gdaldem',  # os.path.join(os.path.dirname(os.path.abspath(__file__)), 'lambda/local/bin/gdaldem'),
        'minZoom': argv[4],
        'maxZoom': argv[5],
        'bestZoom': argv[6],
        'processes': argv[7],
        'sat': 'sat',
        'gdal2tiles_moisbo': os.path.join(os.path.dirname(os.path.abspath(__file__)), 'gdal2tiles_moisbo.py'),
        'logPath': logPath
    }

    griphus.handler(event)


def touch(path):
    with open(path, 'a'):
        os.utime(path, None)


if __name__ == "__main__":
    main()

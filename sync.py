#! /usr/bin/env python

from os.path import exists
import logging
import subprocess
import os


def handler(event):
    # aws s3 sync 'outputPath' s3://infojsons.griphus.com.au/db
    logging.info('sync: {}'.format(event['tmp']))
    if not os.path.exists(event['tmp']):
        logging.error('no output to sync')
        return 'no output to sync'

    command = 'aws s3 sync {} s3://{}/{} --exclude "*" --include "*.png" --acl "public-read"'.format(
        event['tmp'], event['griphus'], event['sat'])

    logging.info(command)

    output = subprocess.check_output(command, shell=True)

    return output

## Griphus Process Program

Open [bash/install.sh]() for installation instructions on debian/ubuntu

Can start with `main.args.py`, `main.test.py` to call `main.py`

main.args.py runs with arguments

```
/mnt/process/griphus-gdal2tiles/main.args.py 56JMQ 30 20 11 18
```


### AWS Lambda

Go to [lambda/lambda.start.py]() to install a function and use Run Command on AWS


### Profiles 

Profiles are used for gdal2tiles_moisbo in griphus2tiles


### Arguments

`/mnt/process/griphus-process/main.args.py 56JMQ 30 20 18 16 -1`

```
argv[1] = 56JMQ # tileName
argv[2] = 30 # dataCoveragePercentage
argv[3] = 20 # cloudyPixelPercentage
argv[4] = 18 # minZoom
argv[5] = 16 # maxZoom
argv[6] = -1 # Number of processes to run (-1 or 0 to use all cores available in the machine)
```


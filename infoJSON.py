from datetime import datetime


class InfoJSON:
    def __init__(self, _tile, _event):
        self.name = '{}{}{}'.format(_tile['utmZone'], _tile['latitudeBand'], _tile['gridSquare'])
        dates = []
        dates.append(TileDate(_tile))
        self.dates = dates
        self.folder = '{}/{}/{}'.format(_tile['utmZone'], _tile['latitudeBand'], _tile['gridSquare'])
        self.description = 'Auto Generated Tile'
        self.zoom = Zoom(_event)
        self.created = datetime.today().strftime('%d %b %Y')
        # for constructor and for other days see that you update this
        self.updated = datetime.today().strftime('%d %b %Y')


class Zoom:
    def __init__(self, _event):
        self.min = _event['minZoom'] or 10
        self.max = _event['maxZoom'] or 16
        self.best = _event['bestZoom'] or 12


class TileDate:
    def __init__(self, _tile):
        replace = 'tiles/{}/{}/{}/'.format(_tile['utmZone'], _tile['latitudeBand'], _tile['gridSquare'])
        # replace = tiles / 56 / J / MP / 2017 / 1 / 1 / 0
        dateTile = _tile['path'].replace(replace, '')  # 2017/1/1/0
        dateTile = dateTile[:-2]
        dateTile = datetime.strptime(dateTile, '%Y/%M/%d')

        self.date = '{}/ndvi-mercator'.format(_tile['path'].replace('tiles', ''))
        self.dataCoverage = _tile['dataCoveragePercentage']
        self.cloudCoverage = _tile['cloudyPixelPercentage']
        self.formatDate = dateTile.strftime('%d %b %Y')
        self.dateName = '{}'.format(dateTile.strftime('%d_%b_%Y'))

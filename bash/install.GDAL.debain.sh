## DEBIAN INSTALL

#!/bin/bash
sudo apt-get update
sudo apt-get install -y build-essential libgdal-dev python-all-dev python-pip vim curl git-core libproj-dev proj-data proj-bin
pip install --upgrade --user awscli
pip install --upgrade awscli
# and in sudo su
pip install --upgrade awscli
sudo pip install boto3
cd /tmp
curl https://s3.amazonaws.com/ec2-downloads-windows/SSMAgent/latest/debian_amd64/amazon-ssm-agent.deb -o amazon-ssm-agent.deb
sudo dpkg -i amazon-ssm-agent.deb
sudo systemctl start amazon-ssm-agent
sudo systemctl enable amazon-ssm-agent

# We need to compile a patched version of libecwj:
cd ~
wget http://meuk.technokrat.nl/libecwj2-3.3-2006-09-06.zip
unzip libecwj2-3.3.2006-09-06.zip
wget http://trac.osgeo.org/gdal/raw-attachment/ticket/3162/libecwj2-3.3-msvc90-fixes.patch
patch -p0 < libecwj2-3.3-msvc90-fixes.patch
wget http://osgeo-org.1560.x6.nabble.com/attachment/5001530/0/libecwj2-3.3-wcharfix.patch
wget http://trac.osgeo.org/gdal/raw-attachment/ticket/3366/libecwj2-3.3-NCSPhysicalMemorySize-Linux.patch
cd libecwj2-3.3/
patch -p0 < ../libecwj2-3.3-NCSPhysicalMemorySize-Linux.patch
patch -p1 < ../libecwj2-3.3-wcharfix.patch

./configure
make
sudo make install

# Download and compile gdal

wget http://download.osgeo.org/gdal/2.1.3/gdal-2.1.3.tar.gz
tar -xzvf gdal-2.1.3.tar.gz
cd gdal-2.1.3
./configure --with-python --with-ecw=/usr/local
make
sudo make install

# test
python -c "from osgeo import gdal;print(gdal.__version__)"
gdalinfo --formats | grep ECW

export LD_LIBRARY_PATH=/usr/local/lib
export PYTHONPATH=/usr/local/lib/python2.7/dist-packages

# add to .bashrc and .profile and /etc/profile to user and sudo

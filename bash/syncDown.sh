#!/bin/bash

aws s3 sync s3://infojsons.griphus.com.au/db $PWD/db/ --acl public-read --delete

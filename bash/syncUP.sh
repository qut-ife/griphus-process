#!/bin/bash

aws s3 sync $PWD/db/ s3://infojsons.griphus.com.au/db --acl public-read --delete

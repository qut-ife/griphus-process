!# /bin/bash

sudo apt-get update
sudo apt-get -y install gdal-bin python-gdal python-pip vim curl git-core
pip install --upgrade --user awscli
sudo pip install boto3

# Run Command
mkdir /tmp/ssm

# To skip the next we could try UBUNTU AWS image

curl https://s3.amazonaws.com/ec2-downloads-windows/SSMAgent/latest/debian_amd64/amazon-ssm-agent.deb -o /tmp/ssm/amazon-ssm-agent.deb
sudo dpkg -i /tmp/ssm/amazon-ssm-agent.deb
sudo service amazon-ssm-agent status
sudo service amazon-ssm-agent start
echo -e '#!/bin/bash \n### BEGIN INIT INFO\n# Provides:          amazon-ssm-agent\n# Required-Start:    $local_fs $network\n# Required-Stop:     $local_fs\n# Default-Start:     2 3 4 5\n# Default-Stop:      0 1 6\n# Short-Description: amazon-ssm-agent\n# Description:       amazon ssm agent\n### END INIT INFO\n\nsystemctl start amazon-ssm-agent' > /etc/init.d/amazon-ssm-agent
sudo chmod +x /etc/init.d/amazon-ssm-agent

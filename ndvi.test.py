#! /usr/bin/env python

import ndvi
import logging
import os


def main():
    formatter = '%(asctime)s - %(levelname)s - %(message)s'
    logging.basicConfig(level=logging.DEBUG, format=formatter)
    logging.info('test ndvi')

    dataPath = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'test-data/tmp/tiles/56/J/MP/2017/2/10/0')
    band4 = os.path.join(dataPath, 'B04.jp2')
    band8 = os.path.join(dataPath, 'B08.jp2')
    out = os.path.join(dataPath, 'NDVIcalc.tif')
    ndvi.create(band4FileLocation=band4, band8FileLocation=band8, outputTifLocation=out)

    # ndvi.allRasterBands(band8)


if __name__ == "__main__":
    main()

#! /usr/bin/env python

import griphus2tiles
import logging
import os


def main():
    formatter = '%(asctime)s - %(levelname)s - %(message)s'
    logging.basicConfig(level=logging.DEBUG, format=formatter)

    logging.info('test griphus2tiles')

    dataPath = 'test-data/tmp/tiles/56/J/MP/2017/2/10/0'

    pyDir = 'lambda/local/lib64/python2.7/dist-packages'
    libDir = 'lambda/local/lib'

    event = {
        'ndviAnalysed': os.path.join(os.path.dirname(os.path.abspath(__file__)), dataPath, 'NDVI-analysed.tif'),
        'outPath': os.path.join(os.path.dirname(os.path.abspath(__file__)), dataPath),
        'minZoom': 11,
        'maxZoom': 11,
        'bestZoom': 12,
        'PYTHONPATH': '',
        'LD_LIBRARY_PATH': '',
        'processes': 0,
        'gdal2tiles_moisbo': os.path.join(os.path.dirname(os.path.abspath(__file__)), 'gdal2tiles_moisbo.py')
    }

    msg = griphus2tiles.handler(event)

    logging.debug(msg)


if __name__ == "__main__":
    main()

#! /usr/bin/env python

import logging
import subprocess
import os


def handler(event):
    logging.info('start griphus2tiles')

    libdir = event['LD_LIBRARY_PATH']
    pydir = event['PYTHONPATH']
    ndviAnalysedLocation = event['ndviAnalysed']
    outPath = os.path.join(event['outPath'] + '/ndvi-mercator/')
    min = int(event['minZoom'])
    max = int(event['maxZoom'])
    processes = str(event['processes'])
    gdal2tiles_moisbo = event['gdal2tiles_moisbo']

    # you can add '--verbose', to the argvs
    if processes == '-1' or processes == '0':
        argv = ['-p', 'mercator', '-z', str(min) + '-' + str(max),
                '"' + ndviAnalysedLocation + '"',
                '"' + outPath + '"']
    else:
        argv = ['--processes=' + str(processes), '-p', 'mercator', '-z', str(min) + '-' + str(max),
                '"' + ndviAnalysedLocation + '"',
                '"' + outPath + '"']

    command = ''
    if libdir == '' or pydir == '':
        command = 'python {} {}'.format(gdal2tiles_moisbo, ' '.join(argv))
    else:
        command = 'PYTHONPATH={} LD_LIBRARY_PATH={} python {} {}'.format(pydir, libdir, gdal2tiles_moisbo,
                                                                         ' '.join(argv))

    logging.info(command)
    output = subprocess.check_output(command, shell=True)

    logging.debug(output)

    return {
        'message': 1
    }

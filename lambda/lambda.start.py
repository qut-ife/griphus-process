import boto3


def handler(event, context):
    event = validateEvent(event)

    ec2 = boto3.resource('ec2')
    ssm = boto3.client('ssm')

    instance = ec2.Instance(event['instanceId'])

    if not instance.public_ip_address:
        return 'Cannot find instance'

    print "Public dns: %s" % instance.public_ip_address

    script = '/home/admin/griphus-process/main.args.py'
    run_script = [script,
                  event['tileName'], event['dataCoveragePercentage'], event['cloudyPixelPercentage'],
                  event['minZoom'], event['maxZoom'], event['bestZoom'],
                  event['processes']]

    print ' '.join(run_script)

    output = ssm.send_command(InstanceIds=[
        event['instanceId']
    ],
        DocumentName='AWS-RunShellScript',
        Parameters={
            "commands": [
                'chmod +x {}'.format(script),
                ' '.join(run_script)
            ],
            "executionTimeout": ["3600"]
        })

    cmdLog = 'Command: %s started' % output['Command']['CommandId']
    print cmdLog

    return cmdLog


def validateEvent(_event):
    try:
        _event['tileName'] = _event['tileName']
        _event['instanceId'] = _event['instanceId']
        _event['dataCoveragePercentage'] = _event['dataCoveragePercentage']
        _event['cloudyPixelPercentage'] = _event['cloudyPixelPercentage']
        _event['minZoom'] = None if _event['minZoom'] == '' else _event['minZoom']
        _event['maxZoom'] = None if _event['maxZoom'] == '' else _event['maxZoom']
        _event['bestZoom'] = None if _event['bestZoom'] == '' else _event['bestZoom']
        _event['processes'] = 0 if _event['processes'] == '' else _event['processes']

        return _event

    except KeyError, e:
        print 'KeyError in event: {}'.format(e)
        raise

import boto3
import time


def handler(event, context):
    # TODO implement
    client = boto3.client('ec2')

    response = client.stop_instances(
        DryRun=False,
        InstanceIds=[
            event['instanceId'],
        ],
        Force=False
    )

    stoppingInstances = next(iter(response['StoppingInstances'] or []), None)

    while stoppingInstances['CurrentState']['Name'] == u'stopped':
        time.sleep(2)
        print stoppingInstances['CurrentState']['Name']

    msg = 'Instance: {} status is {}'.format(event['instanceId'], stoppingInstances['CurrentState']['Name'])
    return msg

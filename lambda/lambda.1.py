import boto3
import subprocess
import time

def handler(event, context):
    ec2 = boto3.resource('ec2')

    user_data_script = """
    #!/bin/bash

    sudo apt-get update
    sudo apt-get -y install gdal-bin python-gdal python-pip
    pip install --upgrade --user awscli
    sudo pip install boto3

    """

    instances = ec2.create_instances(ImageId='ami-09daf96a', KeyName='heemssh',
                                     MinCount=1, MaxCount=1, InstanceType='t2.micro',
                                     SecurityGroups=['default'], UserData=user_data_script,
                                     Placement={'AvailabilityZone': 'ap-southeast-2a'})

    instance = next(iter(instances or []), None)

    ec2.create_tags([instance.id], {"Name": "Griphus Process Test L"})

    print "New instance created."

    while instance.state == u'pending':
        print "Instance state: %s" % instance.state
        time.sleep(10)
        instance.update()

    print "Instance state: %s" % instance.state
    print "Public dns: %s" % instance.public_dns_name

    ec2.attach_volume(VolumeId='vol-057e181c2bbd8efbc', Device='/dev/xvdf')

    mount_script = """
    sudo mkdir /mnt/process
    sudo mount /dev/xvdf /mnt/process
    """
    output = subprocess.check_output(mount_script, shell=True)

    print output

    run_script = """
    sudo python /mnt/process/griphus-gdal2tiles/main.test.py
    """
    output = subprocess.check_output(run_script, shell=True)

    print output




#! /usr/bin/env python

import logging
import os
import boto3
from datetime import date, datetime

event = {
    'tileName': '56JMP',
    'tmp': os.path.join(os.getcwd(), 'test-data/tmp'),
    'griphus': 'infojsons.griphus.com.au',
    'ndviAnalysed': '{}/NDVI-analysed.tif'.format(os.path.join(os.getcwd(), 'test-data/tmp')),
    'status': 'downloaded: {}'.format(date.today().strftime('%d-%b-%Y %Z'))
}


class TileInfoDate:
    def __init__(self):
        self.dateName = '3_6_2017'


def main():
    s3Griphus = boto3.resource('s3')
    griphusBucket = s3Griphus.Bucket(event['griphus'])
    griphusTransfer = boto3.s3.transfer.S3Transfer(s3Griphus.meta.client)

    tileInfoDate = TileInfoDate()

    tileKey = 'tiles/{}/{}'.format(event['tileName'], tileInfoDate.dateName)

    griphusTransfer.upload_file(event['ndviAnalysed'], event['griphus'], '{}.tif'.format(tileKey),
                                extra_args={'Metadata': {'status': event['status'], 'tile': tileKey,
                                                         'ContentType': 'image/tiff'}})

    logging.debug('Uploading TIF {} to {}'.format(tileKey, event['griphus']))


if __name__ == "__main__":
    main()

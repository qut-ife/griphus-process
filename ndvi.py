#! /usr/bin/env python

import struct
from osgeo import gdal
import logging
import sys


def create(band4FileLocation, band8FileLocation, outputTifLocation):
    '''
     INPUT FILES:
     -Sentinel 2 Orthorectified Tile Band 4 (Visible Red) Image - JPEG2000 Format
     -Sentinel 2 Orthorectified Tile Band 8 (Near-Infared) Image - JPEG2000 Format
     OUTPUT FILES:
     -NDVI RAW 8-bit Image with values stored as x100 of scientific output - GeoTIFF Format
    '''

    # GDAL Library open the Band4 and Band8 orthorectfied TILES from Sentinel2 */
    dsb8 = gdal.Open(band8FileLocation, gdal.GA_ReadOnly)
    dsb4 = gdal.Open(band4FileLocation, gdal.GA_ReadOnly)

    ods = output(outputTifLocation, dsb4)

    red_band = dsb4.GetRasterBand(1)  # RED BAND
    nir_band = dsb8.GetRasterBand(1)  # NIR BAND
    # Retrieve the number of lines within the image
    numLines = red_band.YSize
    logging.info('Loop through each line in turn.')
    # Loop through each line in turn.
    for line in range(numLines):
        # Define variable for output line.
        outputLine = ''
        # Read in data for the current line from the
        # image band representing the red wavelength
        red_scanline = red_band.ReadRaster(0, line, red_band.XSize, 1,
                                           red_band.XSize, 1, gdal.GDT_Float32)
        # Unpack the line of data to be read as floating point data
        red_tuple = struct.unpack('f' * red_band.XSize, red_scanline)

        # Read in data for the current line from the
        # image band representing the NIR wavelength
        nir_scanline = nir_band.ReadRaster(0, line, nir_band.XSize, 1,
                                           nir_band.XSize, 1, gdal.GDT_Float32)
        # Unpack the line of data to be read as floating point data
        nir_tuple = struct.unpack('f' * nir_band.XSize, nir_scanline)

        # Loop through the columns within the image
        for i in range(len(red_tuple)):
            # Calculate the NDVI for the current pixel.
            ndvi_lower = (nir_tuple[i] + red_tuple[i])
            ndvi_upper = (nir_tuple[i] - red_tuple[i])
            ndvi = 0
            # Becareful of zero divide
            if ndvi_lower == 0:
                ndvi = 0
            else:
                ndvi = ndvi_upper / ndvi_lower
                # ndvi = int(100 * (ndvi_upper / ndvi_lower))
                # ndvi = 255 if ndvi > 255 else 0 if ndvi < 0 else ndvi
            # Add the current pixel to the output line
            outputLine = outputLine + struct.pack('f', ndvi)
        # Write the completed line to the output image
        # We should be writing out as Bytes
        ods.GetRasterBand(1).WriteRaster(0, line, red_band.XSize, 1,
                                         outputLine, buf_xsize=red_band.XSize,
                                         buf_ysize=1, buf_type=gdal.GDT_Float32)
        # Delete the output line following write
        del outputLine
    logging.info('NDVI Calculated and Outputted to File')

    return 'done'


def output(outFilename, inDataset):
    driver = gdal.GetDriverByName("GTiff")
    # Check that this driver can create a new file.
    metadata = driver.GetMetadata()
    if metadata.has_key(gdal.DCAP_CREATE) and metadata[gdal.DCAP_CREATE] == 'YES':
        logging.info('Driver GTiff supports Create() method.')
    else:
        logging.error('Driver GTIFF does not support Create()')
    # Get the spatial information from the input file
    geoTransform = inDataset.GetGeoTransform()
    geoProjection = inDataset.GetProjection()
    # Create an output file of the same size as the inputted
    # image but with only 1 output image band.
    newDataset = driver.Create(outFilename, inDataset.RasterXSize,
                               inDataset.RasterYSize, 1, gdal.GDT_Float32)
    # Define the spatial information for the new image.
    newDataset.SetGeoTransform(geoTransform)
    newDataset.SetProjection(geoProjection)
    return newDataset


def allRasterBands(fileLocation):
    src_ds = gdal.Open(fileLocation)
    if src_ds is None:
        logging.error('Unable to open' + fileLocation)
        sys.exit(1)

    print "[ RASTER BAND COUNT ]: ", src_ds.RasterCount
    for band in range(src_ds.RasterCount):
        band += 1
        print "[ GETTING BAND ]: ", band
        srcband = src_ds.GetRasterBand(band)
        if srcband is None:
            continue

        stats = srcband.GetStatistics(True, True)
        if stats is None:
            continue

        print "[ STATS ] =  Minimum=%.3f, Maximum=%.3f, Mean=%.3f, StdDev=%.3f" % (
            stats[0], stats[1], stats[2], stats[3])

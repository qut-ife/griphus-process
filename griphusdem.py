#! /usr/bin/env python

from os.path import exists
import logging
import subprocess
import os


def handler(event):

    libdir = event['LD_LIBRARY_PATH']
    gdaldem = event['gdaldem']
    ndvifile = event['ndvifile']
    colorprofile = event['colorprofile']
    demoutfile = event['demoutfile']

    argv = ['color-relief', '-of GTiff', '"' + ndvifile + '"', '"' + colorprofile + '"',
            '"' + demoutfile + '"', '-alpha']

    command = '{} {}'.format(gdaldem, ' '.join(argv))

    logging.info(command)

    output = subprocess.check_output(command, shell=True)

    return output

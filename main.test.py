#! /usr/bin/env python

import main as griphus
import logging
import os


def main():
    baseLogPath = os.path.join(os.path.dirname(__file__), 'logs')
    logPath = os.path.join(baseLogPath, 'griphus.log')

    if not os.path.exists(baseLogPath):
        os.makedirs(baseLogPath)
    if not os.path.exists(logPath):
        touch(logPath)

    formatter = '%(asctime)s - %(levelname)s - %(message)s'

    logging.basicConfig(filename=logPath, filemode='w', level=logging.DEBUG, format=formatter)

    logging.info('test main')

    event = {
        'tileName': '56JMP',
        'tmp': os.path.join(os.getcwd(), 'test-data/tmp'),
        'griphus': 'infojsons.griphus.com.au',
        'key': 'db/allTiles.json',
        'sentinel': 'sentinel-s2-l1c',
        'dataCoveragePercentage': 30,
        'cloudyPixelPercentage': 20,
        'PYTHONPATH': '',  # lambda/local/lib64/python2.7/dist-packages',
        'LD_LIBRARY_PATH': '',  # ''lambda/local/lib',
        'colorprofile': os.path.join(os.path.dirname(__file__), 'color-relief-0-1.txt'),
        'gdaldem': '/Library/Frameworks/GDAL.framework/Programs/gdaldem',
        'minZoom': 11,
        'maxZoom': 15,
        'bestZoom': 12,
        'processes': 0,
        'sat': 'sat'
    }

    griphus.handler(event)


def touch(path):
    with open(path, 'a'):
        os.utime(path, None)


if __name__ == "__main__":
    main()

#! /usr/bin/env python
# ! /usr/bin/env python

import logging
import os
import ndvi
import griphus2tiles
import griphusdem
import boto3
import json
import errno
from datetime import date, datetime
from infoJSON import InfoJSON
import sync
import shutil


def handler(event):
    logging.info('Griphus Main')

    event = validateEvent(event)

    s3EuCentral1 = boto3.resource('s3', 'eu-central-1')
    sentinelBucket = s3EuCentral1.Bucket(event['sentinel'])

    s3Griphus = boto3.resource('s3')
    griphusBucket = s3Griphus.Bucket(event['griphus'])
    griphusTransfer = boto3.s3.transfer.S3Transfer(s3Griphus.meta.client)

    # download json list

    contentObject = s3Griphus.Object(event['griphus'], event['key'])
    allTiles = contentObject.get()['Body'].read().decode('utf-8')
    jsons = json.loads(allTiles)

    # make list
    for j in jsons['tiles']:
        if j['status'] == 'new':
            logging.info(j['location'])
            contentObject = s3EuCentral1.Object(event['sentinel'], j['location'])
            tile = contentObject.get()['Body'].read().decode('utf-8')
            infoJson = json.loads(tile)
            # File Base for output path
            event['outPath'] = '{}/{}'.format(event['tmp'], infoJson['path'])
            # save JSONs downloaded state
            if infoJson['dataCoveragePercentage'] > event['dataCoveragePercentage'] and \
                    infoJson['cloudyPixelPercentage'] < event['cloudyPixelPercentage']:
                pathCreate(event['outPath'])
                for band in range(4, 9, 4):
                    # Download the image locally
                    filePath = '{}/B0{}.jp2'.format(event['outPath'], band)
                    fileToDownload = '{}/B0{}.jp2'.format(infoJson['path'], band)
                    if not os.path.exists(filePath):
                        sentinelBucket.download_file(fileToDownload, filePath)
                event['ndvifile'] = '{}/NDVIcalc.tif'.format(event['outPath'])
                if not os.path.exists(event['ndvifile']):
                    ndvi.create('{}/B0{}.jp2'.format(event['outPath'], 4), '{}/B0{}.jp2'.format(event['outPath'], 8),
                                event['ndvifile'])
                event['demoutfile'] = '{}/NDVI-analysed.tif'.format(event['outPath'])
                if not os.path.exists(event['demoutfile']):
                    griphusdem.handler(event)
                event['ndviAnalysed'] = event['demoutfile']
                griphus2tiles.handler(event)
                j['status'] = 'downloaded: {}'.format(date.today().strftime('%d-%b-%Y %Z'))
                # create a json with details needed
                tileInfo = InfoJSON(infoJson, event)
                tileInfoDate = next(iter(tileInfo.dates or []), None)
                tileKey = 'tiles/{}/{}'.format(event['tileName'], tileInfoDate.dateName)
                tileInfoJson = toJSON(tileInfo)
                logging.info('Uploading JSON {} to {}'.format(tileKey, event['griphus']))
                griphusBucket.put_object(Key='{}.json'.format(tileKey), Body=tileInfoJson,
                                         ContentType='application/json',
                                         ACL='public-read')
                logging.info('Uploading TIF {} to {}'.format(tileKey, event['griphus']))
                griphusTransfer.upload_file(event['ndviAnalysed'], event['griphus'], '{}.ndvi.tif'.format(tileKey),
                                            extra_args={'Metadata': {'status': j['status'], 'tile': tileKey,
                                                                     'ContentType': 'image/tiff'}})
                logging.info('sync sat tiles from {} to {}'.format(event['tmp'], event['griphus']))
                sync.handler(event)
            else:
                j['status'] = 'QA not passed, skipped'
            jsons = writeJSONs(j, jsons)
            # delete
            shutil.rmtree(event['outPath'], ignore_errors=True)

    # upload jsonsList
    griphusBucket.put_object(Key=event['key'], Body=json.dumps(jsons), ContentType='application/json',
                             ACL='public-read')

    logKey = 'logs/griphus.{}.log'.format(datetime.today().strftime('%d-%b-%Y_%H-%M-%S'))

    logging.debug('Uploading {}'.format(event['logPath']))
    griphusBucket.upload_file(event['logPath'], logKey)
    # griphusBucket.put_object(Key=logKey, Body=dataLog, ContentType='application/text', ACL='public-read')

    griphusBucket.put_object(Key='logs/stop', Body='stop', ContentType='application/text')
    return True


def writeJSONs(oneJson, allJsons):
    # find json on jsons list and replace it
    for i, j in enumerate(allJsons['tiles']):
        if j['location'] == oneJson['location']:
            allJsons['tiles'][i] = j

    return allJsons


def pathCreate(path):
    if not os.path.exists(path):
        try:
            os.makedirs(path)
        except OSError as exc:  # Guard against race condition
            if exc.errno != errno.EEXIST:
                logging.error('OSError {}'.format(OSError.message))
                raise


def toJSON(obj):
    return json.dumps(obj, default=lambda o: o.__dict__, sort_keys=True, indent=0)


def validateEvent(_event):
    try:
        _event['tmp'] = '/tmp' if _event['tmp'] == '' else _event['tmp']
        _event['griphus'] = _event['griphus']
        _event['key'] = _event['key']
        _event['sentinel'] = _event['sentinel']
        _event['dataCoveragePercentage'] = int(_event['dataCoveragePercentage'])
        _event['cloudyPixelPercentage'] = int(_event['cloudyPixelPercentage'])
        _event['minZoom'] = None if _event['minZoom'] == '' else int(_event['minZoom'])
        _event['maxZoom'] = None if _event['maxZoom'] == '' else int(_event['maxZoom'])
        _event['bestZoom'] = None if _event['bestZoom'] == '' else int(_event['bestZoom'])
        _event['processes'] = 0 if _event['processes'] == '' else int(_event['processes'])
        _event['logPath'] = _event['logPath']

        return _event
    except KeyError, e:
        logging.error('KeyError in event: {}'.format(e))
        raise

#! /usr/bin/env python

from griphusdem import *
import logging
import os


def main():
    formatter = '%(asctime)s - %(levelname)s - %(message)s'
    logging.basicConfig(level=logging.DEBUG, format=formatter)
    logging.info('test griphusdem')

    dataPath = os.path.join(os.path.dirname(__file__), 'test-data/tmp/tiles/56/J/MP/2017/2/10/0')

    event = {
        'LD_LIBRARY_PATH': '',
        'gdaldem': os.path.join(os.path.dirname(os.path.abspath(__file__)), 'lambda/local/bin/gdaldem'),
        'ndvifile': os.path.join(os.path.dirname(os.path.abspath(__file__)), dataPath, 'NDVIcalc.tif'),
        'colorprofile': os.path.join(os.path.dirname(os.path.abspath(__file__)), 'profiles/color-relief-0-1.txt'),
        'demoutfile': os.path.join(os.path.dirname(os.path.abspath(__file__)), dataPath, 'NDVI-analysed.tif')
    }

    out = handler(event)

    logging.info(out)


if __name__ == "__main__":
    main()

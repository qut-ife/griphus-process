#! /usr/bin/env python

import logging
from infoJSON import InfoJSON

logging.basicConfig(level=logging.DEBUG)


def main():
    logging.info('test info jsons')

    tile = {
        "path": "tiles/56/J/MP/2017/1/1/0",
        "timestamp": "2017-01-01T23:53:08.321Z",
        "utmZone": 56,
        "latitudeBand": "J",
        "gridSquare": "MP",
        "dataCoveragePercentage": 40.16,
        "cloudyPixelPercentage": 22.58,
    }

    tileInfo = InfoJSON(tile)

    assert tileInfo.name == '56JMP'
    assert tileInfo.folder == '56/J/MP'

    dateO = next(iter(tileInfo.dates or []), None)
    assert dateO.formatDate == '01 Jan 2017'
    assert dateO.date == '/56/J/MP/2017/1/1/0/ndvi-mercator'
    assert dateO.dateName == '01_Jan_2017'

if __name__ == "__main__":
    main()
